import { Component, OnInit } from '@angular/core';
import {faEnvelopeOpen} from '@fortawesome/free-solid-svg-icons';
import {faGitlab, faLinkedinIn, faTwitterSquare} from '@fortawesome/free-brands-svg-icons';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {

  faEmail = faEnvelopeOpen;
  faGitLab = faGitlab;
  faTwitter = faTwitterSquare;
  faLinkedIn = faLinkedinIn;

  constructor() { }

  ngOnInit(): void {
  }

}
